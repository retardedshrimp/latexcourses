
\documentclass[11pt,a4paper]{article}

\usepackage{amsmath}
\usepackage{graphicx}
%\usepackage{url}

\title{AMTH142 \hfill Lecture 5\\[6mm]
       \LaTeX{} -- Special Topics}
\author{}

\begin{document}i

\maketitle

\tableofcontents
 
\newpage

\setcounter{section}{4}

\section{Special Topics}

\subsection{Figures and Tables}

\subsubsection{Placement}

Figures and tables generally cannot be broken up, so \LaTeX{} has a
problem whenever it starts a figure or table and reaches the end of
a page before that figure or table is finished.
In such a case, the figure or table will be held over until the page
is finished.
You might also actually prefer a figure or table to appear at either
the top or bottom of a page.
Figures and tables are referred to as \textbf{floats} in \LaTeX{}.

The \texttt{figure} and \texttt{table} environments have
an additional placement specifier, which indicates the allowable
placements of the float.
These are 
\begin{enumerate}
  \item \texttt{h} for \textsl{here}
  \item \texttt{t} for \textsl{top} of a page
  \item \texttt{b} for \textsl{bottom} of a page
  \item \texttt{p} for a special \textsl{page} containing only floats
  \item \texttt{!} for \textsl{try really hard} to follow my placement
\end{enumerate}

A figure could be started, for example, by
\begin{verbatim}
  \begin{figure}[!ht]
\end{verbatim}
which tells \LaTeX{} to try hard to place the figure here, or if
that is not possible at the top of a page.

The placement of floats is a common problem with \LaTeX{}, see
NSSI \S2.12 for more information on this.

\subsubsection{Tables}

The \texttt{table} environment is quite distinct form the
\texttt{tabular} environment, although the latter is often
used within the \texttt{table} environment.
For small tables there is usually no problem with placement, 
but larger tables should always be enclosed in a
\verb+table+ environment.

\subsubsection*{Example:}
This example simply takes the table from Lecture 4 and encloses it in a
\verb+table+ environment.

\newpage

\begin{verbatim}
  \begin{table}[!ht]
    \begin{center}
      \begin{tabular}{|l||cl|}
        \hline
        Name & Date & Formula \\
        \hline
        Newton   & 1687 & $F = m a$ \\
        Einstein & 1905 & $E = m c^2$ \\ 
        \hline
      \end{tabular}
    \end{center}
  \end{table}
\end{verbatim}

{\small 
  \begin{table}[!ht]
    \begin{center}
      \begin{tabular}{|l||cl|}
        \hline
        Name & Date & Formula \\
        \hline
        Newton   & 1687 & $F = m a$ \\
        Einstein & 1905 & $E = m c^2$ \\ 
        \hline
      \end{tabular}
    \end{center}
  \end{table}
}

\subsubsection{Captions}

Captions can be added to floats with the \verb+\caption+
command.
The caption can be made to appear at either the top or bottom of 
the float by the placement of the \verb+\caption+ command.
Figures and tables are numbered and can be referenced using
\verb+\label+ and \verb+\ref+ as explained in \S 3.1.1.

\subsubsection*{Example:}

\begin{verbatim}
  \begin{table}[!ht] 
    \caption{Physics Formulas} \label{tbl:physics}
    \begin{center}
      \begin{tabular}{|l||cl|}
        \hline
        Name & Date & Formula \\
        \hline
        Newton   & 1687 & $F = m a$ \\
        Einstein & 1905 & $E = m c^2$ \\ 
        \hline
      \end{tabular}
    \end{center}
  \end{table}

  Two famous formulas from physics are shown are shown in 
  Table \ref{tbl:physics}.
\end{verbatim}

{\small 
  \begin{table}[!ht] 
    \caption{Physics Formulas} \label{tbl:physics}
    \begin{center}
      \begin{tabular}{|l||cl|}
        \hline
        Name & Date & Formula \\
        \hline
        Newton   & 1687 & $F = m a$ \\
        Einstein & 1905 & $E = m c^2$ \\ 
        \hline
      \end{tabular}
    \end{center}
  \end{table}

  Two famous formulas from physics are shown are shown in 
  Table \ref{tbl:physics}.
}


\subsection{Including Graphics}

Figures typically contain graphics from other sources.
The most useful format for graphics is eps (encapsulated
postscript) which can easily be incorporated into
pdf documents.
Scilab and most other programs producing graphs
can save graphs in this format (as a \texttt{.eps} file).

There are a number of ways of including graphics in \LaTeX{},
we will use the \texttt{graphicx} package, so you will need
to include
\begin{verbatim}
  \usepackage{graphicx}
\end{verbatim}
in the preamble.

\subsubsection*{Example:}

This example shows how to include an eps file from Scilab
called \verb+brown.eps+:

\begin{verbatim}
  \begin{figure}[!ht]
    \begin{center}
      \includegraphics[angle=270, width=0.7\textwidth]{brown.eps}
      \caption{Brownian Motion}
    \end{center}
  \end{figure}
\end{verbatim}

\begin{figure}[!ht]
  \begin{center}
    %\includegraphics[angle=270, width=0.7\textwidth]{brown.eps} 
    \caption{Brownian Motion}
  \end{center}
\end{figure}

The \verb+\includegraphics+ command has the following optional
controls

\begin{center}
  \begin{tabular}{|l|l|}
   \hline
   \texttt{width} & scale to specified width \\
   \texttt{height} & scale to specified height \\
   \texttt{angle} & rotate counterclockwise \\
   \texttt{scale} & scale  \\
   \hline
  \end{tabular}
\end{center}

For \texttt{.eps} files from different sources, you usually need
to experiment with these to get things right.

In the example above we used \verb+angle=270+ because Scilab
graphs are presented in landscape form and \verb+width=0.7\textwidth+ 
to scale so that its width is 0.7 times the width of the text on the page.

When a graph won't appear where you want it to  it is usually
because the graph is too large to fit in the available space.
Often scaling can be used to shrink the graph so that it fits into 
the available space.


\subsection{Bibliographies}

Bibliographies can be produced using the \texttt{thebibliography}
environment.
Items in the bibliography begin with the \verb+\bibitem+ command
(similar to \verb+\item+ in list environments) followed by a marker
which can then be used with the \verb+\cite+ command to refer to 
the bibliographic item.
Bibliographies are placed at the end of documents (and headed
References in the \texttt{article} document class). 
For large bibliographies it is worth learning about
the \texttt{bibtex} package.

The bibliography at the end of this lecture was produced by:

\begin{verbatim}
  \begin{thebibliography}{99}
    \bibitem{NSSI} Tobias Oetiker et. al. \emph{The Not So 
       Short Introduction to \LaTeXe{}}.
    \bibitem{AMSM} American Mathematical Society, 
      \emph{User's Guide for the \texttt{amsmath} Package}.
  \end{thebibliography}
\end{verbatim}

The \verb+{99}+ in this example tells \LaTeX{} that no bibliographic
item numbers will be \textsl{no wider} than the number 99.

The following shows how bibliographic items can be cited:

\subsubsection*{Example:}

\begin{verbatim}
  Equations can be aligned using either the \texttt{eqnarray}
  environment, see \cite{NSSI} \S3.5, or the \texttt{align}
  environment, see \cite{AMSM} \S3.6.
\end{verbatim}

{\small 
  Equations can be aligned using either the \texttt{eqnarray}
  environment, see \cite{NSSI} \S3.5, or the \texttt{align}
  environment, see \cite{AMSM} \S3.6.
}

\subsection{Macros}

\textbf{Macros} are used to extend \LaTeX{}.
These include \verb+\newcommand+ for defining new
commands,  \verb+\newenvironment+ for defining
new environments. 
The \verb+amsmath+ package has \verb+\DeclareMathOperator+ 
for defining new maths operators like \verb+\cos+.

A typical use is when we need to repeat a \LaTeX{} construction
a number of times.
Including such a construction as a macro has the advantages
of (a) often saving typing, and (b) ensuring the construction
is done exactly the same way every time.

It is good practice to collect all macros 
together, either at the beginning of the document or in a
separate file included with an \verb+\include+ command.
See NSSI \S6.1 for more on macros.

\subsubsection*{Example:}

Suppose we want to write ``Schr\"{o}dinger equation'' many 
times in a document.
We define a new command \verb+\Seqn+ do this:

\begin{verbatim}
  \newcommand{\Seqn}{Schr\"{o}dinger equation}
\end{verbatim}
Now we can use the \verb+\Seqn+ command, but we have to be careful
to follow it immediately by \verb+{}+ to get spacing correct.

\newcommand{\Seqn}{Schr\"{o}dinger equation}

\begin{verbatim}
  The \Seqn{} is the basis of quantum mechanics.
\end{verbatim}

{\small 
  The \Seqn{} is the basis of quantum mechanics.
}


%\subsubsection*{Example:}

%New commands can take \textsl{arguments} to allow more flexibility.
%Suppose, for example, we want to frequently write a sequence like
%$x_1, x_2, \ldots, x_n$.

%\begin{verbatim}
%  \newcommand{\Seq}[2]{#1_{1}, #1_{2}, \ldots , #1_{#2}}
%\end{verbatim}
%will do this.

%\newcommand{\Seq}[2]{#1_{1}, #1_{2}, \ldots ,#1_{#2}}

%The \texttt{[2]} in the \verb+\newcommand+ indicates
%that it takes two arguments.
%Within the \verb+newcommand+
%the arguments are then referred to as \verb+#1+ and \verb+#2+.
%When the new command is applied the actual arguments to
%the command are substituted for \verb+#1+ and \verb+#2+
%as illustrated below:

%\begin{verbatim}
%  The sequence $\Seq{x}{n}$ is equal to the sequence 
%  $\Seq{y}{m}$ when $m = n$ and $x_i = y_i$ for 
%  $i = 1, 2, \ldots n$.
%\end{verbatim}

%{\small 
%  The sequence $\Seq{x}{n}$ is equal to the sequence $\Seq{y}{m}$
%  when $m = n$ and $x_i = y_i$ for $i = 1, 2, \ldots n$.
%}

%\medskip

%\noindent Notes:

%\begin{enumerate}
%  \item In choosing a name for new a command you need to make
%    sure that it is not the same as the name of an existing command.
%  \item It is good practice to collect all macros 
%    together, either at the beginning of the document or in a
%    separate file included with an \verb+\include+ command.
%\end{enumerate}

\subsection{More \LaTeX{}}

In these lectures I have tried to give an outline of the
most important and useful features of \LaTeX{}. 
All the assignments for this unit will be written using \LaTeX{},
so here are some tips for learning more about \LaTeX{}:

\begin{enumerate}
  \item \LaTeX{} is best learned through practice, and the more
    practice the better.
    Try to use \LaTeX{} as much as possible, for example try
    writing assignments for other subjects in \LaTeX{}. 
  \item \LaTeX{} is far too large to learn all at once, it is
    more important to have a familiarity with general features
    of \LaTeX{}, know where to look things up,
    and to understand the possibilities and limitations of \LaTeX{}.
  \item There are numerous topics in NSSI which we haven't covered
    in these lectures. I suggest at least glancing through NSSI to
    get an idea of what more can be done in \LaTeX{}, for example
    indexing, footnotes, theorem environments and so on.
%    Ditto for the \texttt{amsmath} package.
  \item There are numerous \LaTeX{} packages for specialized areas.
    If, for example, you need to write one or two chemical formulas
    you can probably do a reasonable job using what we have learned
    about mathematical formulas.
    However if you needed to do this as part of your work, then it
    it would be very worthwhile to find out about packages for chemical
    formulas, since they will allow you to do the job much
    better and more quickly.
  \item The \texttt{.tex} files for all the lectures, practicals
     and assignments for this unit are available on the web pages.
     This is useful when you see something in the course material
     and want to find out how it was done.
     A lot of other information on the web, especially mathematical
     and scientific documents and information about \LaTeX{} itself
     are also available as \texttt{.tex} files.
\end{enumerate}


  \begin{thebibliography}{99}
    \bibitem{NSSI} Tobias Oetiker et. al. \emph{The Not So Short
       Introduction to \LaTeXe{}}.
    \bibitem{AMSM} American Mathematical Society, 
      \emph{User's Guide for the \texttt{amsmath} Package}.
  \end{thebibliography}


\end{document}
