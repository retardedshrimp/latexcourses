
\documentclass[11pt,a4paper]{article}

\usepackage{amsmath}
%\usepackage{graphicx}
%\usepackage{url}

\title{AMTH142 \hfill Lecture 3\\[6mm]
       \LaTeX{} -- Formatting Mathematics}
\author{}

\begin{document}

\maketitle

\tableofcontents

\newpage

\setcounter{section}{2}

\section{Formatting Mathematics}

\subsection{Mathematics Modes}

There are two mathematics modes in \LaTeX{}:
\begin{enumerate}
  \item Mathematics within text is enclosed between \verb+\(+ and
    \verb+\)+, or between \verb+$+ and \verb+$+ or between
    \verb+\begin{math}+ and \verb+\end{math}+.
    This is often referred to as paragraph mode.
  \item Mathematics displayed on a separate line is enclosed 
    between \verb+\[+ and \verb+\]+, or between \verb+$$+ 
    and \verb+$$+ or between \verb+\begin{displaymath}+ 
    and \\ \verb+\end{displaymath}+.
    This is often referred to as display mode.
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  Here is a formula 
  $x^2 + y^2 = z^2$
  within a paragraph.
  Here is the same formula 
  $$x^2 + y^2 = z^2$$
  in display mode.
\end{verbatim}

{\small 
  Here is a formula 
  $x^2 + y^2 = z^2$
  within a paragraph.
  Here is the same formula 
  $$x^2 + y^2 = z^2$$
  in display mode.  }

\subsubsection*{Whitespace}

There are some important differences between mathematics mode and text:

\begin{enumerate}
  \item Most spaces and line breaks have no significance in mathematics
    mode.
  \item Blank lines are not allowed in mathematics mode.
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  You can type a formula in a way that is almost most unreadable,
  but as long as there are no blank lines it is OK to \LaTeX{}
  $$          x       ^   
              2  +   y  
              ^ 
                     2=z 
      ^                             2 $$ 
\end{verbatim}

{\small 
  You can type a formula in a way that is almost most unreadable,
  but as long as there are no blank lines it is OK to \LaTeX{}
  $$          x       ^   
              2  +   y  
              ^ 
              2=z 
      ^ 2 $$ }

\subsubsection{Numbered Equations}

The pair \verb+\begin{equation}+ and
\verb+\end{equation}+ are used to obtain numbered equations.
When equations are numbered, that numbering can be used to
refer to particular equations.
\LaTeX{} has simple mechanism for handling this: equations can
be labelled with \verb+\label{...}+ and then referred to
with \verb+\ref{...}+.

\subsubsection*{Example:}

\begin{verbatim}
  Here is a numbered equation
  \begin{equation}
    x^2 + y^2 = z^2 .
  \end{equation}
  When an equation has been labelled
  \begin{equation} \label{eq:pythag}
    \sin^2 \theta + \cos^2 \theta = 1
  \end{equation}
  it can be referred to in the text, in this case as Equation
  (\ref{eq:pythag}).
\end{verbatim}

{\small 
  Here is a numbered equation
  \begin{equation}
    x^2 + y^2 = z^2 .
  \end{equation}
  When an equation has been labelled
  \begin{equation} \label{eq:pythag}
    \sin^2 \theta + \cos^2 \theta = 1
  \end{equation}
  it can be referred to in the text, in this case as Equation
  (\ref{eq:pythag}). }

\subsection{Basics}

\subsubsection{Mathematics Fonts}

Mathematical symbols are generally printed in italics.
The dollar signs around mathematics takes care of this 
automatically so use \verb+$x$+ rather that \verb+\textit{x}+.
The \verb+\mathbf+ command is used to produce bold maths symbols 
which are often used for vector and matrices.
These are identical to bold roman text letters produced by
\verb+\textbf+ and are not italicized.

%The other mathematics fonts which you need to know about are
%\verb+\mathbf+ and \verb+mathcal+.

%\begin{enumerate}
%  \item \verb/\mathrm{...}/  $\mathrm{A x = b}$
%  \item \verb/\mathsf{...}/  $\mathsf{A x = b}$
%  \item \verb/\mathtt{...}/  $\mathtt{A x = b}$
%  \item \verb/\mathit{...}/  $\mathit{A x = b}$
%  \item \verb/\mathbf{...}/  $\mathbf{A x = b}$
%  \item \verb/\mathcal{...}/ $\mathcal{A x = b}$
%\end{enumerate}


\subsubsection*{Example:}

\begin{verbatim}
Mathematical symbols like $A$, $x$ and $b$ are the same as 
italic letters \textit{A}, \textit{x} and \textit{b}, but obey 
different spacing rules as in $A x = b$ and \textit{A x = b}.
Numbers look the same whether in maths mode or not,
e.g $123.456$ is the same as 123.456.
\end{verbatim}

{\small 
Mathematical symbols like $A$, $x$ and $b$ are the same as 
italic letters \textit{A}, \textit{x} and \textit{b}, but obey 
different spacing rules as in $A x = b$ and \textit{A x = b}.
Numbers look the same whether in maths mode or not,
e.g $123.456$ is the same as 123.456.
}

\subsubsection{Greek Letters}

\begin{enumerate}
  \item Lowercase Greek letters are referred to by their name,
    e.g. \verb+\alpha+, \verb+\beta+, \verb+\gamma+ \ldots
  \item Uppercase Greek letters are referred to by their name
    with the first letter capitalized, e.g. \verb+\Gamma+, 
    \verb+\Delta+, \verb+\Lambda+ \ldots
  \item Greek letters can only be used in mathematics mode,
    not in ordinary text.
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  $$ V = \frac{4}{3} \pi r^3 $$
  To use a Greek letter like $\Sigma$ in ordinary text we have
  to be in mathematics mode.
\end{verbatim}

{\small 
  $$ V = \frac{4}{3} \pi r^3 $$ 
  To use a Greek letter like $\Sigma$ in ordinary text we have
  to be in mathematics mode.}

\subsubsection{Exponents and Subscripts}

\begin{enumerate}
  \item Exponents and superscripts are specified by a caret \verb+^+.
  \item Subscripts are specified by an underscore \verb+_+.
  \item Exponents and subscripts are usually enclosed in
    braces \verb+{...}+. 
    However when the exponent or subscript is a single
    character the braces are not necessary.
  \item Exponents and subscripts may be mixed and/or nested.
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  If you forget the braces you can get unintended results.  
  For example compare
  $$ X_{ab} = y^{12}  \qquad X_ab = y^12 $$
  Here are the right and wrong ways to nest exponents and 
  subscripts.
  $$ e^{x^{2}} \qquad {e^{x}}^{2}  $$
  $$ P_{a_{0}} \qquad {P_{a}}_{0}  $$
  Here are some examples of mixed exponents and subscripts:
  $$ A_{ij}^{3} \qquad A^{3}_{ij} \qquad 3^{- P_{0}} 
       \qquad P_{x^{3}} $$ 
\end{verbatim}

{\small 
  If you forget the braces you can get unintended results.  
  For example compare
  $$ X_{ab} = y^{12}  \qquad X_ab = y^12 $$
  Here are the right and wrong ways to nest exponents and 
  subscripts.
  $$ e^{x^{2}} \qquad {e^{x}}^{2}  $$
  $$ P_{a_{0}} \qquad {P_{a}}_{0}  $$
  Here are some examples of mixed exponents and subscripts:
  $$ A_{ij}^{3} \qquad A^{3}_{ij} \qquad 3^{- P_{0}} \qquad P_{x^{3}} $$ 
}

\subsubsection{Fractions and Roots}

\subsubsection*{Example:}

\begin{verbatim}
  Fractions are written with the \verb+\frac{...}{...}+ 
  command. Here are some examples:
  $$ \frac{n!}{(n-k)! k!} \qquad 2^{\frac{1}{2}} 
       \qquad \frac{3^5}{4^5} $$
  Sometimes it is preferable to use the slash form, e.g $1/2$,
  as it can be easier to read in some contexts.
  Compare
  $$ x^{\frac{3}{4}} \qquad \text{to} \qquad x^{3/4} $$
  and compare $\frac{3}{4}$ hour to $3/4$ hour.
\end{verbatim}

{\small 
  Fractions are written with the \verb+\frac{...}{...}+ command.
  Here are some examples:
  $$ \frac{n!}{(n-k)! k!} \qquad 2^{\frac{1}{2}} 
       \qquad \frac{3^5}{4^5} $$
  Sometimes it is preferable to use the slash form, e.g $1/2$,
  as it can be easier to read in some contexts.
  Compare
  $$ x^{\frac{3}{4}} \qquad \text{to} \qquad x^{3/4} $$
  and compare $\frac{3}{4}$ hour to $3/4$ hour.  }

\subsubsection*{Example:}

\begin{verbatim}
  Here is how we write square roots $\sqrt{b^2 - 4ac}$ 
  and other roots $\sqrt[127]{2}$.
\end{verbatim}

{\small 
  Here is how we write square roots $\sqrt{b^2 - 4ac}$ 
  and other roots $\sqrt[127]{2}$.  }

\subsubsection{Standard Functions}

The names of certain standard mathematical functions and
abbreviations are obtained by putting a backslash \verb+\+
before their name. See the list on page 51 of NSSI.

\subsubsection*{Example:}

\begin{verbatim}
  $$ \lim_{x \rightarrow 0} \frac{\sin x}{x} = 1 $$
  but if we forget the backslash we get
  $$ \lim_{x \rightarrow 0} \frac{sin x}{x} = 1 $$
\end{verbatim}

{\small 
  $$ \lim_{x \rightarrow 0} \frac{\sin x}{x} = 1 $$
  but if we forget the backslash we get
  $$ \lim_{x \rightarrow 0} \frac{sin x}{x} = 1 $$
}

\subsubsection{Integrals, Sums, Products}

\begin{enumerate}
  \item Integrals are generated by \verb+\int+
  \item Sums are generated by \verb+\sum+
  \item Products are generated by \verb+\prod+
  \item Limits of integration etc. are generated by superscripts
    and subscripts.
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  $$ \int \sin x dx = - \cos x \qquad 
       \int_{0}^{\infty} e^{-x} dx = 1 $$

  $$ \sum_{k=1}^n k =  \frac{1}{2} n (n + 1) \qquad
       \prod_{\text{$k$ even}} P_k = 1 $$

  Integrals, $\int \sin x dx = - \cos x$, sums, 
  $\sum_{k=1}^n k =  \frac{1}{2} n (n + 1)$, and products
  look different within paragraph mode.
\end{verbatim}

{\small 
  $$ \int \sin x dx = - \cos x \qquad 
       \int_{0}^{\infty} e^{-x} dx = 1 $$

  $$ \sum_{k=1}^n k =  \frac{1}{2} n (n + 1) \qquad
       \prod_{\text{$k$ even}} P_k = 1 $$ 

  Integrals, $\int \sin x dx = - \cos x$, sums, 
  $\sum_{k=1}^n k =  \frac{1}{2} n (n + 1)$, and products
  look different within paragraph mode. }

\subsubsection{Derivatives}

\begin{enumerate}
  \item Derivatives are easily constructed using 
    \verb+\frac+
  \item Alternatively, they can be written using the prime symbol 
    \texttt{'}.
  \item The partial derivative symbol is \verb+\partial+
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  $$ \frac{d^2 y}{d x^2} + y(x) = 0 \qquad
       y'' + y = 0 \qquad
       \frac{\partial^2 \phi}{\partial x^2} +
       \frac{\partial^2 \phi}{\partial y^2} = 0 $$

  Again, the slash form, $d\sin x/ dx = \cos x$, is
  is sometimes preferable to the fraction form, 
  $\frac{d\sin x}{dx} = \cos x$, in paragraph mode.
\end{verbatim}

{\small 
  $$ \frac{d^2 y}{d x^2} + y(x) = 0 \qquad
       y'' + y = 0 \qquad
       \frac{\partial^2 \phi}{\partial x^2} +
       \frac{\partial^2 \phi}{\partial y^2} = 0 $$

  Again, the slash form, $d\sin x/ dx = \cos x$, is
  is sometimes preferable to the fraction form, 
  $\frac{d\sin x}{dx} = \cos x$, in paragraph mode.  }

\subsubsection{Accents}

There are a lot of these so make sure you use the
right one for your particular need.

\begin{enumerate}
  \item $\displaystyle \overline{x}$   \verb+  $\overline{x}$+
  \item $\displaystyle \hat{x}$        \verb+  $\hat{x}$+
  \item $\displaystyle \check{x}$      \verb+  $\check{x}$+
  \item $\displaystyle \tilde{x}$      \verb+  $\tilde{x}$+
  \item $\displaystyle \acute{x}$      \verb+  $\acute{x}$+
  \item $\displaystyle \grave{x}$      \verb+  $\grave{x}$+
  \item $\displaystyle \dot{x}$        \verb+  $\dot{x}$+
  \item $\displaystyle \ddot{x}$       \verb+  $\ddot{x}$+
  \item $\displaystyle \breve{x}$      \verb+  $\breve{x}$+
  \item $\displaystyle \bar{x}$        \verb+  $\bar{x}$+
  \item $\displaystyle \vec{x}$        \verb+  $\vec{x}$+
  \item $\displaystyle \mathring{x}$   \verb+  $\mathring{x}$+
  \item $\displaystyle \underline{x}$  \verb+  $\underline{x}$+
\end{enumerate}

\subsubsection{Brackets}

For mathematical formulas to look right brackets must be the
correct size.
\LaTeX{} will determine the correct size bracket if the opening
bracket of a pair is preceded by \verb+\left+ and the closing
bracket is preceded by \verb+\right+.
Curly brackets are written \verb+\{+ and \verb+\}+.

\subsubsection*{Example:}

\begin{verbatim}
  $$ \left[\sum_{k=0}^n \left(x_k - 
       \bar{x} \right)^2 \right]^{\frac{1}{2}} \qquad
       [\sum_{k=0}^n (x_k - \bar{x})^2]^{\frac{1}{2}} $$
\end{verbatim}

{\small 
  $$ \left[\sum_{k=0}^n \left(x_k - 
       \bar{x} \right)^2 \right]^{\frac{1}{2}} \qquad
       [\sum_{k=0}^n (x_k - \bar{x})^2]^{\frac{1}{2}} $$ }

\subsubsection{Spacing}

A number of examples have already used \verb+\qquad+ to separate
formulas on one line. A \verb+\qquad+ is double the space of a
\verb+\quad+.

Another use of spacing is to adjust the position of symbols in
formulas; sometimes small changes can make a big improvement.
These are most often needed with integrals.
The spacings available are:

\begin{enumerate}
  \item \verb+\!+ -- negative thinspace
  \item \verb+\,+ -- thinspace
  \item \verb+\:+ -- medspace
  \item \verb+\;+ -- thickspace
\end{enumerate}


\subsubsection*{Example:}

\begin{verbatim}
  $$ \int_a^b f(x) \, dx  \qquad \qquad \int_a^b f(x) dx $$

  $$ \int \!\!\! \int f(x,y) \, dx \, dy \qquad \qquad
       \int \int f(x,y) dx dy  $$
\end{verbatim}

{\small 
  $$ \int_a^b f(x) \, dx  \qquad \qquad \int_a^b f(x) dx $$

  $$ \int \!\!\! \int f(x,y) \, dx \, dy \qquad \qquad
       \int \int f(x,y) dx dy  $$
}

\subsubsection{Mathematical Symbols}

There is a huge array of mathematical symbols available in
\LaTeX{}.
See the tables on pages 60--66 of NSSI or \verb+symbols.pdf+
in the directory for this lecture.
You should at least have a glance at these to see what is
available.

\subsubsection{Including Text}

Text can be included in mathematical formulas by
using the \verb+\text{...}+ command. 
This is part of the \texttt{amsmath} package and is
preferable to the \verb+\mbox+ of standard \LaTeX{}.

\subsubsection*{Example:}

\begin{verbatim}
  $$ f(x) > 0 \quad \text{for all $x \in X$} $$

  $$ \epsilon_{\text{mach}} \approx 2.2 \times 10^{-16} $$
\end{verbatim}

{\small 
  $$ f(x) > 0 \quad \text{for all $x \in X$} $$

  $$ \varepsilon_{\text{mach}} \approx 2.2 \times 10^{-16} $$ }

\subsection{The \texttt{amsmath} Package}

This package makes available a number of features including:

\begin{enumerate}
  \item A large number of additional mathematical symbols.
  \item Easy to use matrix facility.
  \item A variety of methods for aligning equations.
  \item An easy way of adding new function names.
\end{enumerate}

To access the package include
\begin{verbatim}
  \usepackage{amsmath}
\end{verbatim}
in the preamble.
We will use features of this package in the next lecture.


\end{document}
