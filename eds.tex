
\documentclass[11pt,a4paper]{article}

\usepackage{amsmath}
\usepackage{graphicx}
%\usepackage{url}

\title{AMTH142 \hfill Lecture 1\\[6mm]
       Introduction to \LaTeX{}}

\begin{document}

\maketitle

\tableofcontents

\newpage

\section{Introduction to \LaTeX{}}

The first five lectures of the unit will be on \LaTeX{}.
The lecture topics will be:

\begin{enumerate}
  \item Introduction to \LaTeX{}
  \item Formatting Text
  \item Mathematical Formulas
  \item Tables and Arrays
  \item Special Topics
\end{enumerate}

The reference for these lectures is \textsl{The Not So Short Introduction
to \LaTeXe{}} by Tobias Oetiker and others (see \LaTeX{} Resources on the
web page).
In these lectures it will be referred to by the abbreviation NSSI.

\subsection{How \LaTeX{} Works}

There are three steps in producing a document using \LaTeX{}:

\begin{enumerate}
  \item Create a \LaTeX{} input file. This contains the text and
    formatting commands and must be a plain text (ASCII) file
    with the extension \texttt{.tex}.
  \item Run the file through \LaTeX{}. This produces a \texttt{.dvi}
    (device independent) file which may be viewed, for example,
    by \texttt{xdvi} on Linux systems.
  \item Convert the \texttt{.dvi} file to form which can be printed.
    Typically this will result in a \texttt{.pdf} (pdf -- portable
    document format) file.
\end{enumerate}

The mechanics of doing all this will be covered in the first practical.
The rest of this lecture will cover the structure of \LaTeX{} input
files.

\subsection{\LaTeX{} Input Files}

\subsubsection{Spaces}

\begin{enumerate}
  \item One or more \textsl{whitespace} characters such as spaces,
    tabs or linebreaks are treated as single space.
  \item One or more blank lines start a new paragraph.
\end{enumerate}

\subsubsection*{Example:}

Note: Examples in these notes will usually take the form of \LaTeX{}
input in typewriter text, followed by the result in slightly smaller type.

\begin{verbatim}
                            This is
               a silly 
                          way                   to type
        a                                      sentence.


              Followed      by                        a
   new                            paragraph.
\end{verbatim}

{\small This is a silly way to type a sentence.

 Followed by  a new paragraph.}

\subsubsection{Special Characters}

Some characters have a special meaning and will not print as expected.
The most important of these are:
\begin{verbatim}

  #   $   %   &   {   }   \

\end{verbatim}
Except for the backslash itself, these can be printed by
preceding them with a backslash. 
The \verb/$\backslash$/ command is used to print a backslash.

\subsubsection*{Example:}

\begin{verbatim}
  \#   \$   \%   \&   \{   \}   $\backslash$
\end{verbatim}

{\small \#   \$   \%   \&   \{   \}   $\backslash$}

\subsubsection{Comments}

A \texttt{\%} is used for comments.
When \LaTeX{} encounters a \texttt{\%} the rest of the line is ignored.

\subsubsection*{Example:}

\begin{verbatim}
The rest of this line will be ignored, % THIS IS A COMMENT
% and another comment
as will the line above.
\end{verbatim}

{\small The rest of this line will be ignored, % THIS IS A COMMENT
% and another comment
as will the line above.}

\subsubsection{\LaTeX{} Commands}

\LaTeX{} commands begin with a backslash \verb/\/. 
A big part of learning \LaTeX{} consists of learning its commands and their
effects.

\subsubsection*{Example:}

\begin{verbatim}
\LaTeX{} is the topic of this lecture.
\end{verbatim}

{\small \LaTeX{} is the topic of this lecture.}

\subsection{Input File Structure}

\LaTeX{} input files must follow a certain structure:

\begin{enumerate}
  \item Each input file must begin with a command
    \begin{verbatim}
      \documentclass{...}
    \end{verbatim}
  The document class determines the overall layout of the document.
  The \LaTeX{} document classes are:
  \begin{description}
    \item{\texttt{article}:} This is the only one you will need for this unit.
    \item{\texttt{report}:} This is used for longer documents containing
         several chapters, e.g. PhD theses.
    \item{\texttt{book}:} For books.
    \item{\texttt{slides}:} For overhead projector slides.
    \item{\texttt{letter}:} For letters.
  \end{description}

  \item After that follows commands which influence the style of the
    document or load packages which add new capabilities to \LaTeX{}.
    This is often referred to as the preamble.
  \item The body of the text is started with
    \begin{verbatim}
      \begin{document}
    \end{verbatim}
  \item Next follows the text of the document itself interspersed with
    \LaTeX{} commands.
  \item The whole thing is terminated with
    \begin{verbatim}
      \end{document}
    \end{verbatim}
\end{enumerate}

\subsubsection*{Example:}

The following is a small but complete input file for a \LaTeX{} document:

\begin{verbatim}
\documentclass{article}
\begin{document}
  This is a very short article.
\end{document}
\end{verbatim}

\end{document}
