
\documentclass[11pt,a4paper]{article}

\usepackage{amsmath}
\usepackage{graphicx}
%\usepackage{url}

\title{AMTH142 \hfill Lecture 2\\[6mm]
       \LaTeX{} -- Formatting Text}
\author{}

\begin{document}

\maketitle

\tableofcontents

\newpage

\setcounter{section}{1}

\section{Formatting Text}

\subsection{Special Symbols}

\subsubsection{Quotation Marks}

\begin{enumerate}
  \item For quotation marks use \verb/``/ for opening quotes and \verb/''/ for 
    closing quotes.
  \item For single quotes use one of each.
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
   Do you mean ``eye'' or `i'?
\end{verbatim}

{\small Do you mean ``eye'' or `i'?}

\subsubsection{Dashes and Hyphens}

  There are three types of dashes in \LaTeX{}.

\subsubsection*{Example:}

\begin{verbatim}
   1 - short-dashes and hyphens

   2 -- long--dashes

   3 --- longer---dashes
\end{verbatim}

{\small 
   1 - short-dashes and hyphens

   2 -- long--dashes

   3 --- longer---dashes}

\subsection{Font Selection}

\subsubsection{Font Types}

The font types generally available in \LaTeX{} are:

\begin{enumerate}
  \item \verb/\textrm{...}/ \textrm{roman}
  \item \verb/\texttt{...}/ \texttt{typewriter}
  \item \verb/\textsl{...}/ \textsl{slanted}
  \item \verb/\textsf{...}/ \textsf{sans serif}
  \item \verb/\textbf{...}/ \textbf{bold face}
  \item \verb/\textit{...}/ \textit{italic}
  \item \verb/\textsc{...}/ \textsc{small capitals}
  \item \verb/\emph{...}/ \emph{emphasized}
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  This \textit{sentence} \texttt{uses} a \textsl{number} of 
  \textsf{different} \textbf{fonts} \textit{which} 
  \textsc{makes} it \textbf{hard} to \textsc{read}. \\
  \emph{Emphasized text} differs from \textit{italic text}
  in that \textsf{it can be \emph{combined} with other font 
  changes.}
\end{verbatim}

{\small 
  This \textit{sentence} \texttt{uses} a \textsl{number} of 
  \textsf{different} \textbf{fonts} \textit{which} 
  \textsc{makes} it \textbf{hard} to \textsc{read}. \\
  \emph{Emphasized text} differs from \textit{italic text}
  in that \textsf{it can be \emph{combined} with  other font changes.}
}

\subsubsection{Font Sizes}

The font size, either 10pt (the default), 11pt or 12pt, for the whole
document is set within the initial \verb/\documentclass/ command, e.g.
\begin{verbatim}
  \documentclass[12pt]{article}
\end{verbatim}
The font size and type of title and section headings are chosen
automatically by \LaTeX{}.

The font sizes generally available in \LaTeX{} are:

\begin{enumerate}
  \item  \verb/{\tiny ...}/ {\tiny tiny}
  \item  \verb/{\scriptsize ...}/ {\scriptsize very small}
  \item  \verb/{\footnotesize ...}/ {\footnotesize quite small}
  \item  \verb/{\small ...}/ {\small small}
  \item  \verb/{\normalsize ...}/ {\normalsize normal}
  \item  \verb/{\large ...}/ {\large larger}
  \item  \verb/{\Large ...}/ {\Large larger still}
  \item  \verb/{\LARGE ...}/ {\LARGE quite large}
  \item  \verb/{\huge ...}/ {\huge very large}
  \item  \verb/{\Huge ...}/ {\Huge huge}
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  When combining changes of font {\Large \textbf{size and 
  type}}, remember that the \textbf{size} 
  change comes first.
\end{verbatim}

{\small
  When combining changes of font {\Large \textbf{size and 
  type}}, remember that the \textbf{size} 
  change comes first.
}


\subsection{Spacing and Indentation}

\subsubsection{Paragraphs and Indentation}

We have already seen that in \LaTeX{} a blank line starts a new paragraph.
By default \LaTeX{} indents each paragraph except the first paragraph
of a Chapter, Section etc. 
This can be controlled using the commands \verb/\indent/ and \verb/\noindent/.

\subsubsection*{Example:}

\begin{verbatim}
  Normally paragraphs are indented.

  \noindent But this one isn't.
\end{verbatim}

{\small 

  Normally paragraphs are indented.

\noindent But this one isn't.}

\subsubsection{Line and Page Breaks}

\begin{enumerate}
  \item The commands \verb/\\/ or \verb/\newline/ force a new line
    to be started without starting a new paragraph.
  \item The command \verb/\newpage/ can be used to force a new page
    to be started.
\end{enumerate}

\subsubsection*{Example:}

\begin{verbatim}
  This is how to start a new line \\
  without starting a new paragraph.

  Of course, a new paragraph is started by a blank line.
\end{verbatim}

{\small
  This is how to start a new line \\
  without starting a new paragraph.

  Of course, a new paragraph is started by a blank line.}

\subsubsection{Spacing Between Paragraphs}

By default \LaTeX{} adds no extra space between paragraphs.
Sometimes, to make certain paragraphs stand out, you need to
add extra space. This can be done with the \verb/\smallskip/,
\verb/\medskip/ and \verb/\bigskip/ commands.

\subsubsection*{Example:}

\begin{verbatim}
  Here is an example of \ldots

  different spacings \ldots

  \smallskip

  between paragraphs.

  \medskip

  This is useful in highlighting certain paragraphs.

  \bigskip

  It is also useful with equations, tables and diagrams.
\end{verbatim}


{\small 
  Here is an example of \ldots

  different spacings \ldots

  \smallskip

  between paragraphs.

  \medskip

  This is useful in highlighting certain paragraphs.

  \bigskip

  It is also useful with equations, tables and diagrams.
}

\subsection{Sections and Subsections}

The sectioning commands
\begin{verbatim}
     \section{...}
     \subsection{...}
     \subsubsection{...}
\end{verbatim}
are available in the \texttt{article} document class.
The additional command \verb/\chapter/ is available in the
\verb/report/ and \verb/book/ document classes.

The numbering of sections is done automatically by \LaTeX{}, as
is the font selection for titles and spacing between sections.

The
\begin{verbatim}
     \subsubsection*{...}
\end{verbatim}
command does not print the subsubsection number. 

\subsubsection*{Example}

\begin{verbatim}
  \subsubsection*{Example}

  This is how examples are introduced in these notes.
\end{verbatim}

{\small
  \subsubsection*{Example}

  This is how examples are introduced in these notes.
}

\subsection{Titles and Tables of Contents}

The following example gives the first few lines of \textsl{this}
document\footnote{You usually need to run a document through \LaTeX{} twice
to get the table of contents correct.}:

\subsubsection*{Example:}

\begin{verbatim}
\documentclass[11pt,a4paper]{article}

\title{AMTH142 \hfill Lecture 2\\[6mm]
       \LaTeX{} -- Formatting Text}
\author{}

\begin{document}

\maketitle

\tableofcontents

\newpage

\section{Formatting Text}

\subsection{Special Symbols}

\subsubsection{Quotation Marks}
\end{verbatim}

\subsection{Environments}

These are generally associated with a pair of matching commands
\begin{verbatim}
  \begin{...}
  \end{...}
\end{verbatim}

\subsubsection{Lists}

\LaTeX{} has three types of list environments:

\begin{enumerate}
  \item \texttt{enumerate}
  \item \texttt{itemize}
  \item \texttt{description}
\end{enumerate}

The individual items in the list are introduced by the \verb/\item/
command. List can be nested, that is you can have lists within
lists.

\subsubsection*{Example:}

\begin{verbatim}
  \begin{enumerate}
    \item The \texttt{enumerate} environment numbers the 
      elements in the list.
    \item The \texttt{itemize} environment precedes each 
      item by a large dot as follows:
      \begin{itemize}
        \item This is the first item of an \texttt{itemize}
          environment.
        \item And this is the second.
      \end{itemize}
    \item This is an example of the \texttt{description} 
      environment.
      \begin{description}
        \item[First] item in the list.
        \item[Second] item in the list.
      \end{description}
  \end{enumerate}
\end{verbatim}

{\small 
  \begin{enumerate}
    \item The \texttt{enumerate} environment numbers the elements in
      the list.
    \item The \texttt{itemize} environment precedes each item by
      a large dot as follows:
      \begin{itemize}
        \item This is the first item of an \texttt{itemize}
           environment.
        \item And this is the second.
      \end{itemize}
    \item This is an example of the \texttt{description} environment.
      \begin{description}
        \item[First] item in the list.
        \item[Second] item in the list.
      \end{description}
  \end{enumerate}
}

\subsubsection{Centering Text}

\subsubsection*{Example:}

\begin{verbatim}
  \begin{center}
     This is an example \\ of centered \\ text. \\
     Centering is useful when including tables and diagrams.
  \end{center}
\end{verbatim}

{\small 
  \begin{center}
     This is an example \\ of centered \\ text. \\
     Centering is useful when including tables and diagrams.
  \end{center} }

\subsubsection{Verbatim}

Text enclosed between a \verb/\begin{verbatim}/ and \verb/\end{verbatim}/
pair is printed exactly as is in typewriter font, including spaces and
linebreaks, and with \LaTeX{} commands ignored.

\subsubsection*{Example:}

\verb/  \begin{verbatim}/ \\
\verb/    \LaTeX{} commands are ignored in verbatim environments,/ \\
\verb/    but spaces   and/ \\
\verb/    linebreaks are faithfully followed./ \\
\verb/  \end{verbatim}/
 
{\small 
  \begin{verbatim}
    \LaTeX{} commands are ignored in verbatim environments,
    but spaces   and
    linebreaks are faithfully followed.
  \end{verbatim} }

The \texttt{verbatim} environment is used for the examples in these notes.
The same effect within paragraphs can be obtained with the 
\verb=\verb= command.
The character immediately following the \verb/\verb/ is
the delimiting character; the following text will be printed
verbatim until this delimiting character is reached again.

\subsubsection*{Example:}

%\begin{verbatim}
%  To print \verb+\verb+ we need to use \verb/\verb+\verb+/,
%  since \verb+\texttt{\verb}+ will try to execute a 
%  \verb+\verb+ command while using \verb+typewriter+ font. 
%  In this example I have used \verb/+/ and \verb+/+ as 
%  delimiters.
%\end{verbatim}

%{\small 
%  To print \verb+\verb+ we need to use \verb/\verb+\verb+/,
%  since \verb+\texttt{\verb}+ will try to execute a 
%  \verb+\verb+ command while using \verb+typewriter+ font. 
%  In this example I have used \verb/+/ and \verb+/+ as delimiters.
%}

\begin{verbatim}
  An important difference between \verb+\verb+ and 
  \verb+\texttt+ is that \LaTeX{} commands have their intended 
  effect inside \verb+\texttt+, while inside \verb+\verb+ they 
  are printed verbatim. 
  In this example I have used \texttt{+} as the delimiter.
\end{verbatim}

{\small 
  An important difference between \verb+\verb+ and \verb+\texttt+
  is that \LaTeX{} commands have their intended effect inside 
  \verb+\texttt+, while inside \verb+\verb+ they are printed verbatim. 
  In this example I have used \texttt{+} as the delimiter.
}
\end{document}
